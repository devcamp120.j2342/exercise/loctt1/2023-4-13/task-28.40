"use strict";
$(document).ready(function(){
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gREADY_STATE_REQUEST_DONE = 4;
  var gSTATUS_REQUEST_DONE = 200;
  var gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  $("#btn-check-voucher").on("click", onBtnKiemTraClick);
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //hàm xử lý sự kiện khi nút kiểm tra được click
  function onBtnKiemTraClick(){
    "use strict"
    var vInpVoucher = $("#voucher"); //truy xuất phần tử inp voucher
    var vDivResultCheck = $("#div-result-check");

    console.log("%c subtask 1", "color: red");
    console.log("Tôi lấy dữ liệu từ");
    console.log("Input, id = " + vInpVoucher.attr("id") + " - placeholder: " + vInpVoucher.attr("placeholder"));
    console.log("Tác động đến");
    console.log("Div, id = " + vDivResultCheck.attr("id") + "- innerHTML = " + vDivResultCheck.html());

    //khai báo đối tượng  voucher để sử dụng trong các bước
    var vVoucherObj = {
      voucherCode: ""
    }
    //bước 1: Thu thập dữ liệu 
    var vVoucherCode = readData(vVoucherObj);

    //bước 2: kiểm tra dữ liệu 
    var vIsValidateData = validateData(vVoucherObj);
    //bước 3: gọi sever
    var vXmlHttp = new XMLHttpRequest();
    goiAPIDeLayVoucher(vVoucherObj, vXmlHttp);
    //bước 4: xử lý hiển thị dữ liệu
    vXmlHttp.onreadystatechange = function(){
      if(vXmlHttp.readyState == gREADY_STATE_REQUEST_DONE && vXmlHttp.status === gSTATUS_REQUEST_DONE){
        hienThiVoucher(vXmlHttp);
      }
      else if(vXmlHttp.readyState == gREADY_STATE_REQUEST_DONE){
        var vDivResultCheck = $("#div-result-check");
        vDivResultCheck.html("Mã giảm giá: " + vDivResultCheck.voucherCode +  "không tồn tại");
      }
    }
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //hàm thu thập dữ liệu 
   function readData(paramVoucherObj){
    "use strict"
    var vInpVoucher = $("#voucher");
    var vInpVoucherValue = vInpVoucher.val();
    paramVoucherObj.voucherCode = vInpVoucherValue;
   }

   //hàm kiểm tra dữ liệu 
   function validateData(paramVoucherObj){
    "use strict"
    var vDivResultCheck = $("#div-result-check");
    if(paramVoucherObj.voucherCode == ""){
      //hiển thị câu thông báo lõi ra màn hình 
      vDivResultCheck.html("Bạn cần nhập mã giảm giá vào!");

      //thay đổi chữ thành màu đỏ 
      vDivResultCheck.prop("class", "text-danger");
      return false;
    }
    else
    {
      //thay đổi chữ thành màu đen bình thường 
      vDivResultCheck.prop("class", "text-dark");
      vDivResultCheck.html("");
      return true;
    }
    
   }

   //hàm gọi api để lấy mã giảm giá 
   function goiAPIDeLayVoucher(paramVoucherObj, paramXmlHttp){
    paramXmlHttp.open("GET", gBASE_URL + paramVoucherObj.voucherCode, true);
    paramXmlHttp.send();
   }

   //hàm xử lý việc hiển thị mã voucher
   function hienThiVoucher(paramXmlHttp){
    "use strict"
    //bước 1: nhận lại response dạng json ở xmlHttp.responseText
    var vJsonVoucherResponse = paramXmlHttp.responseText;

    //lấy responseText ghi ra console.log
    console.log(vJsonVoucherResponse);

    // chuyển về object 
     var vVoucherResObj = JSON.parse(vJsonVoucherResponse);

     //xử lý mã giảm giá dựa vào đối tượng vừa có 
     var vDiscount = vVoucherResObj.phanTramGiamGia;
     var vDivResultCheck = $("#div-result-check");
     vDivResultCheck.html("Mã giảm giá: " + vDiscount + "%");
   }
});

